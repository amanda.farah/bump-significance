"Data loading helper functions"

import glob
import os
import h5py
import numpy as np
import pandas as pd
import xarray as xr

def load_meta_file_from_hdf5(filename, labels, keep_metadata=False):
    """
    Load the posterior from a `hdf5` `PESummary` file.
    See `load_posterior_from_meta_file`.
    """
    new_style = True
    with h5py.File(filename, "r") as data:
        if "posterior_samples" in data.keys():
            new_style = False
            data = data["posterior_samples"]
        label = list(data.keys())[0]
        for _label in labels:
            if _label in data.keys():
                label = _label
                break
        if new_style:
            posterior = pd.DataFrame(data[label]["posterior_samples"][:])
        elif hasattr(data[label], "keys"):
            posterior = pd.DataFrame(
                data[label]["samples"][:],
                columns=[key.decode() for key in data[label]["parameter_names"][:]],
            )
        else:
            posterior = pd.DataFrame(data[label][:])
        if keep_metadata:
            for key in data[label]["meta_data"]['meta_data'].keys():
                posterior[key]=data[label]["meta_data"]['meta_data'][key][()][0]\
                        * np.ones_like(posterior['mass_1'])
        return posterior, label


def load_posterior_from_meta_file(filename, labels=None, original_keys=False,
                                  keep_metadata=False):
    """
    Load a posterior from a `PESummary` meta file.

    Parameters
    ----------
    filename: str
    labels: list
        The labels to search for in the file in order of precedence.

    Returns
    -------
    posterior: pd.DataFrame
    meta_data: dict
        Dictionary containing the run label that was loaded.

    """
    _mapping = dict(
        mass_1="mass_1_source",
        mass_2="mass_2_source",
        mass_ratio="mass_ratio",
        redshift="redshift",
        a_1="a_1",
        a_2="a_2",
        cos_tilt_1="cos_tilt_1",
        cos_tilt_2="cos_tilt_2",
    )
    if labels is None:
        labels = ["Mixed","PrecessingSpinIMRHM", "PrecessingSpin"]
    if not os.path.exists(filename):
        raise FileNotFoundError(f"{filename} does not exist")
    _posterior, label = load_meta_file_from_hdf5(filename=filename,
                                                 labels=labels,
                                                 keep_metadata=keep_metadata)
    if original_keys:
        posterior = pd.DataFrame(_posterior)
    else:
        posterior = pd.DataFrame({key: _posterior[_mapping[key]] for key in _mapping})
    meta_data = dict(label=label)
    print(f"Loaded {label} from {filename}.")
    return posterior, meta_data


def load_batch_of_meta_files(regex, label, labels=None, original_keys=False,
                             keep_metadata=False):
    keys = [
        "mass_1",
        "mass_2",
        "mass_ratio",
        "a_1",
        "a_2",
        "cos_tilt_1",
        "cos_tilt_2",
        "redshift",
    ]
    posteriors = dict()
    meta_data = dict()
    all_files = glob.glob(regex)
    print(f"Found {len(all_files)} {label} events in standard format.")
    for posterior_file in all_files:
        try:
            new_posterior, data = load_posterior_from_meta_file(
                posterior_file, labels=labels,
                original_keys=original_keys, keep_metadata=keep_metadata
            )
        except TypeError as e:
            print(f"Failed to load {posterior_file} with TypeError: {e}.")
            continue
        if all([key in new_posterior for key in keys]):
            meta_data[posterior_file] = data
            if not original_keys:
                new_posterior = new_posterior[keys]
            if min(new_posterior["mass_ratio"]) >= 1:
                new_posterior["mass_ratio"] = 1 / new_posterior["mass_ratio"]
            posteriors[posterior_file.split('/')[-1]] = new_posterior
        else:
            print(f"Posterior has keys {new_posterior.keys()}.")
    return posteriors

def load_O3_events(path_O3a='/Users/amandafarah/proj/LIGO_data/GWTC-2/GW*',
                   path_O3b='/Users/amandafarah/proj/LIGO_data/GWTC-3/GW*',
                   BBH_only=False,
                   original_keys=False,
                   labels=None,
                   keep_metadata=False,
                   far_cut=None,
                   gwosc_data_path='/Users/amandafarah/proj/LIGO_data/GWTC-3/gwosc_download_gwtc3.csv'
                  ):
    """ Loads in PE data from O3a and O3b data releases, combines them, and
    returns the result as an xarray dataset. By default returns all events,
    including the NSBH and BNS events.
    """
    gwtc_2 = load_batch_of_meta_files(path_O3a, "O3a",
                                      original_keys=original_keys,
                                      labels=labels,
                                      keep_metadata=keep_metadata)
    gwtc_3 = load_batch_of_meta_files(path_O3b, "O3b",
                                      original_keys=original_keys,
                                      labels=labels,
                                      keep_metadata=keep_metadata)

    if BBH_only:
        # remove NSBH and BNS events
        not_bbhs = ['GW200105', 'GW200115', 'GW190425', 'GW191219']
        not_bbh_keys = list()
        for key in gwtc_2.keys():
            if any([n in key for n in not_bbhs]):
                not_bbh_keys.append(key)
        [gwtc_2.pop(key) for key in not_bbh_keys]
        not_bbh_keys = list()
        for key in gwtc_3.keys():
            if any([n in key for n in not_bbhs]):
                not_bbh_keys.append(key)
        [gwtc_3.pop(key) for key in not_bbh_keys]
    if far_cut is not None:
        print('cutting on FAR > ',far_cut)
        # remove below-threshold events
        gwosc_dat = pd.read_csv(gwosc_data_path)
        labels_to_drop = list(
            gwosc_dat[gwosc_dat['far'] > far_cut]['id'].apply(lambda x: x[:-3]))
        catalog_name = list(gwosc_dat[gwosc_dat['far'] > far_cut]['catalog.shortName'])
        drop_labels_gwtc2=[]
        drop_labels_gwtc3=[]
        for i in range(len(labels_to_drop)):
            if catalog_name[i] == "GWTC-1-confident" or "GWTC-2" in catalog_name[i]:
                for key in gwtc_2.keys():
                    if labels_to_drop[i] in key:
                        drop_labels_gwtc2.append(key)
                        print(key, 'dropped')
            elif catalog_name[i] == "GWTC-3-confident":
                for key in gwtc_3.keys():
                    if labels_to_drop[i] in key:
                        drop_labels_gwtc3.append(key)
                        print(key, 'dropped')
        [gwtc_2.pop(k) for k in drop_labels_gwtc2]
        [gwtc_3.pop(k) for k in drop_labels_gwtc3]

	# combine the two datasets
    events = dict()
    events.update(gwtc_2)
    events.update(gwtc_3)

    # put it in the only useful format
    events_ds = xr.Dataset(events).rename(dict(dim_0='PE_sample',dim_1='parameter'))
    return events_ds

def load_GWTC1_events(path_gwtc1='/Users/amandafarah/proj/LIGO_data/GWTC-1/GW*',
                      BBH_only=False,
                      original_keys=False,
                     ):
    """ Loads in PE data from GWTC-1 data release, which includes O1 and O2.
    Returns the result as an xarray dataset. By default returns all events
    (including the BNS).
    """
    files = glob.glob(path_gwtc1)

    # add redshift posterior
    z_array = np.expm1(np.linspace(np.log(1), np.log(10 + 1), 1000))
    from astropy import units
    from astropy.cosmology import Planck15
    from scipy.interpolate import interp1d
    distance_array = Planck15.luminosity_distance(z_array).to(units.Mpc).value
    z_of_d = interp1d(distance_array, z_array)

    posteriors = dict()
    meta_data = dict()
    for filename in files:
        posterior = pd.DataFrame()
        approximant = "Overall"
        if 'GW170817' in filename:
            approximant = "IMRPhenomPv2NRT_highSpin"
            if BBH_only:
                continue
        with h5py.File(filename, "r") as ff:
            try:
                data = np.array(ff[f"{approximant}_posterior"])
            except KeyError as e:
                print(f"Failed to load {filename} with KeyError: {e}")
                continue
            if original_keys:
                posterior = pd.DataFrame(data)
            else:
                # add redshift posterior
                posterior["redshift"] = z_of_d(data["luminosity_distance_Mpc"])
                # rename keys to make them match what is expected
                for ii in [1, 2]:
                    posterior[f"mass_{ii}"] = data[f"m{ii}_detector_frame_Msun"] / (
                        1 + posterior["redshift"]
                    )
                    posterior[f"a_{ii}"] = data[f"spin{ii}"]
                    posterior[f"cos_tilt_{ii}"] = data[f"costilt{ii}"]
                # add mass ratio posterior
                posterior["mass_ratio"] = posterior["mass_2"] / posterior["mass_1"]
        posteriors[filename.split('/')[-1]] = posterior
        meta_data[filename] = dict(label="GWTC-1", approximant=approximant)
        print(f"Loaded {approximant} from {filename}.")

    # put it in the only useful format
    return xr.Dataset(posteriors).rename(
        dict(dim_0='PE_sample',dim_1='parameter')
    )

def load_all_events(path_gwtc1='/Users/amandafarah/proj/LIGO_data/GWTC-1/GW*',
                    path_O3a='/Users/amandafarah/proj/LIGO_data/GWTC-2/GW*',
                    path_O3b='/Users/amandafarah/proj/LIGO_data/GWTC-3/GW*',
                    BBH_only=False,
                    original_keys=False,
                    labels=None,
                    keep_metadata=False,
                    far_cut=None,
                    gwosc_data_path='/Users/amandafarah/proj/LIGO_data/GWTC-3/gwosc_download_gwtc3.csv'
                   ):

    O3_evs = load_O3_events(path_O3a, path_O3b, BBH_only, original_keys,
                            labels=labels, keep_metadata=keep_metadata,
                            far_cut=far_cut,gwosc_data_path=gwosc_data_path)
    O1O2_evs = load_GWTC1_events(path_gwtc1, BBH_only,original_keys,
                                 far_cut=far_cut,gwosc_data_path=gwosc_data_path)

    all_evs = xr.merge([O1O2_evs, O3_evs])
    return all_evs

def load_from_gwpop_output(
    path_sample_file='/Users/amandafarah/proj/LIGO_data/GWTC-3-population-data/analyses/PowerLawPeak/o1o2o3_mass_c_iid_mag_iid_tilt_powerlaw_redshift_samples.hdf5',
    group='reweighted'
):
    """ Load the PE samples of all events in the population study and return the result
    as an xarray Dataset
    """
    import deepdish as dd

    if group=='reweighted':
        name_mapping = {
                'phony_dim_2':'event',
                'phony_dim_3':'PE_sample'}

    elif group=='original':
        name_mapping = {
                'phony_dim_0':'event',
                'phony_dim_1':'PE_sample'}
    else:
        print("group not regocnized!")
    # load in all the samples
    pe_samps = xr.load_dataset(path_sample_file,
                               group=group,
                               engine='netcdf4'
                              ).rename(name_mapping)

    # correct bug introduced in GWTC2
    pe_samps['mass_2']=pe_samps['mass_ratio']*pe_samps['mass_1']

    # add event names
    dd_pe_samps = dd.io.load(path_sample_file)
    nms = dd_pe_samps['names']

    # rename the sample array so that it can be indexed by name
    pe_samps = pe_samps.assign_coords(dict(event=nms))

    # change the format so it matches what we get when we load PE samples from
    # the data release. Also, this way makes more sense in general.
    # We make each event an entry into the dataset, and the PE sample number 
    # and parameter are coordinates.
    flipped = xr.Dataset()
    for ev in pe_samps.event:
        flipped = flipped.assign({
            str(ev.values): pe_samps.sel(event=ev).to_array().rename({"variable":"parameter"})
        })

    return flipped.assign_coords(dict(PE_sample=flipped.PE_sample))
